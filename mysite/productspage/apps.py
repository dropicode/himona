from django.apps import AppConfig


class ProductspageConfig(AppConfig):
    name = 'productspage'
