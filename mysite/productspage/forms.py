from django.forms import ModelForm
from .models import ClothingProducts
from django import forms

class AddProductToCart(ModelForm):

    class Meta:
        model = ClothingProducts
        fields = ['product_id', 'size', 'color', 'selling_price']
        quantity = forms.IntegerField(label="quantity")

