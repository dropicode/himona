from django.contrib import admin
from .models import MainCat, ClothingCat, ClothingProducts, CharacterCat, CharacterProducts, NoncharacterCat, NoncharacterProducts
import copy
# Register your models here.


class MainCatAdmin(admin.ModelAdmin):
    list_display = ['main_cat', 'main_cat_chi']
    prepopulated_fields = {'slug': ('main_cat',)}
    search_fields = ['main_cat', 'main_cat_chi']


class ClothingCatAdmin(admin.ModelAdmin):
    list_display = ['main_cat', 'secondary_cat', 'secondary_cat_chi']
    prepopulated_fields = {'slug': ('secondary_cat',)}
    search_fields = ['main_cat', 'secondary_cat', 'secondary_cat_chi']


class CharacterCatAdmin(admin.ModelAdmin):
    list_display = ['main_cat', 'secondary_cat', 'secondary_cat_chi']
    prepopulated_fields = {'slug': ('secondary_cat',)}
    search_fields = ['main_cat', 'secondary_cat', 'secondary_cat_chi']


class NoncharacterCatAdmin(admin.ModelAdmin):
    list_display = ['main_cat', 'secondary_cat', 'secondary_cat_chi']
    prepopulated_fields = {'slug': ('secondary_cat',)}
    search_fields = ['main_cat', 'secondary_cat', 'secondary_cat_chi']


def copy_course(modeladmin, request, queryset):

    for course in queryset:
        # multiple could be selected
        course_copy = copy.copy(course)  # django copy object
        course_copy.id = None  # set 'id' to None to create new object
        # Clear out values
        course_copy.building = None
        course_copy.room = None
        course_copy.template = False
        course_copy.template_course = course
        course_copy.signup_count = 0
        course_copy.attendance_count = 0
        course_copy.credit = False
        course_copy.cancelled = False
        course_copy.deleted = False
        course_copy.processed = False
        course_copy.processed_date = None
        course_copy.approved = False
        course_copy.approved_user = None
        course_copy.approved_notes = None
        course_copy.date_start = None
        course_copy.date_end = None
        course_copy.save()  # initial save

        # copy M2M relationship: prerequisites
        #for prerequisite in course.prerequisite_courses.all():
        #    course_copy.prerequisite_courses.add(prerequisite)

        course_copy.save()  # (7) save the copy
        copy_course.short_description = "Duplicate Course"


class ClothingProductsAdmin(admin.ModelAdmin):
    list_display = ['product_id', 'product_id_unique', 'gender', 'img1', 'price', 'discount', 'selling_price', 'stock', 'available', 'updated_at', 'preorder_quantity']
    list_filter = ['available', 'updated_at', 'gender', 'secondary_cat', 'preorder']
    prepopulated_fields = {'slug': ('product_id',)}
    list_editable = ['price', 'discount', 'stock', 'available']
    search_fields = ['product_id', 'product_id_unique']
    actions = [copy_course]


class CharacterProductsAdmin(admin.ModelAdmin):
    list_display = ['product_id', 'product_id_unique', 'gender', 'img1', 'price', 'discount', 'selling_price', 'stock', 'available', 'updated_at', 'preorder_quantity']
    list_filter = ['available', 'updated_at', 'gender', 'secondary_cat', 'preorder']
    prepopulated_fields = {'slug': ('product_id',)}
    list_editable = ['price', 'discount', 'stock', 'available']
    search_fields = ['product_id', 'product_id_unique']
    actions = [copy_course]


class NoncharacterProductsAdmin(admin.ModelAdmin):
    list_display = ['product_id', 'product_id_unique', 'gender', 'img1', 'price', 'discount', 'selling_price',
                    'stock', 'available', 'updated_at', 'preorder_quantity']
    list_filter = ['available', 'updated_at', 'gender', 'secondary_cat', 'preorder']
    prepopulated_fields = {'slug': ('product_id',)}
    list_editable = ['price', 'discount', 'stock', 'available']
    search_fields = ['product_id', 'product_id_unique']
    actions = [copy_course]


admin.site.register(MainCat, MainCatAdmin)
admin.site.register(ClothingCat, ClothingCatAdmin)
admin.site.register(CharacterCat, CharacterCatAdmin)
admin.site.register(NoncharacterCat, NoncharacterCatAdmin)
admin.site.register(ClothingProducts, ClothingProductsAdmin)
admin.site.register(CharacterProducts, CharacterProductsAdmin)
admin.site.register(NoncharacterProducts, NoncharacterProductsAdmin)

