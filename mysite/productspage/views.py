from django.shortcuts import render
from django.views import View
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import View
from .models import MainCat, ClothingCat, ClothingProducts, CharacterCat, CharacterProducts, NoncharacterCat, NoncharacterProducts
from .forms import AddProductToCart
from django.core.paginator import Paginator
from datetime import date, timedelta, datetime
import pytz
import sys
sys.path.append('../')
from cartpage.models import Invoice, InvoiceDetails, PromoCode
# Create your views here.


class ProductspageView(View):
    template_name = "productspage/productspage.html"

    def get(self, request, *args, **kwargs):
        request.session["current"] = "products"
        main_cat_slug = str(self.kwargs["main_cat_slug"]).lower()
        main_cat_list = MainCat.objects.order_by('-main_cat').filter(main_cat=main_cat_slug)
        secondary_cat_slug = str(self.kwargs["secondary_cat_slug"])
        if main_cat_slug == "fashion":
            product_type = ClothingProducts
            secondary_cat_list = ClothingCat.objects.order_by('-secondary_cat')
            gender_list = [
                ('lady', '女士'),
                ('gentleman', '男士'),
                ('children', '小童'),
                ('others', '其他')
            ]
        elif main_cat_slug == 'characters':
            product_type = CharacterProducts
            secondary_cat_list = CharacterCat.objects.order_by('-secondary_cat')
            gender_list = [
                ('lifestyle', '生活百貨'),
                ('accessory', '其他飾品'),
                ('stationary', '文具精品'),
                ('school', '校園生活'),
                ('workspace', '工作空間'),
            ]
        elif main_cat_slug == 'noncharacters':
            product_type = NoncharacterProducts
            secondary_cat_list = NoncharacterCat.objects.order_by('-secondary_cat')
            gender_list = [
                ('home', '家居'),
                ('bedroom', '睡房'),
                ('kitchen', '廚房'),
                ('washroom', '洗手間'),
                ('stationary', '文具精品'),
                ('school', '校園生活'),
                ('workspace', '工作空間'),
            ]

        gender_slug = str(self.kwargs["gender_slug"]).lower()
        label_slug = str(self.kwargs["label_slug"]).lower()
        label_list = [('trending', '℃ ↑'), ('sale', '%OFF'), ('preorder', '(pre-order)'), ('none', 'all')]

        outstanding_invoice = Invoice.objects.order_by('-created_at').filter(outstanding=True)
        for i in outstanding_invoice:
            p1 = i
            date_format = "%d/%m/%Y"
            utc = pytz.UTC
            date_limit1 = datetime.now() - timedelta(hours=48)
            date_limit = utc.localize(date_limit1)
            if i.created_at < date_limit:
                article = Invoice.objects.filter(invoice=i.invoice).get()
                if article.cancel == True:
                    pass
                else:
                    invoice_detail_item = InvoiceDetails.objects.order_by('product_id_unique').filter(
                        details_invoice__invoice__contains=i.invoice)
                    for j in invoice_detail_item:
                        if j.product_id_unique[0:3] == "FAS":
                            Products = ClothingProducts
                        elif j.product_id_unique[0:3] == 'CHA':
                            Products = CharacterProducts
                        elif j.product_id_unique[0:3] == 'NON':
                            Products = NoncharacterProducts
                        product_details = Products.objects.filter(product_id_unique=j.product_id_unique)[0]
                        if j.preorder == False:
                            product_details.stock = product_details.stock + j.quantity
                        else:
                            product_details.preorder_quantity = product_details.preorder_quantity - j.quantity
                        product_details.save()
                    article.cancel = True
                    article.save()
            else:
                pass









        if secondary_cat_slug != "all" and gender_slug != "all" and label_slug != 'none':
            products = product_type.objects.order_by('-created_at').filter(main_cat=main_cat_slug).filter(
                secondary_cat__secondary_cat=secondary_cat_slug).filter(gender=gender_slug).filter(label=label_slug).filter(diff_size_only=False)
        elif secondary_cat_slug == "all" and gender_slug == "all" and label_slug == 'none':
            products = product_type.objects.order_by('-created_at').filter(main_cat=main_cat_slug).filter(diff_size_only=False)
        #!=sec
        elif secondary_cat_slug != "all" and gender_slug == "all" and label_slug == 'none':
            products = product_type.objects.order_by('-created_at').filter(main_cat=main_cat_slug).filter(
                secondary_cat__secondary_cat=secondary_cat_slug).filter(diff_size_only=False)
        #!=gender
        elif secondary_cat_slug == "all" and gender_slug != "all" and label_slug == 'none':
            products = product_type.objects.order_by('-created_at').filter(main_cat=main_cat_slug).filter(
                gender=gender_slug).filter(diff_size_only=False)
        #!=label
        elif secondary_cat_slug == "all" and gender_slug == "all" and label_slug != 'none':
            products = product_type.objects.order_by('-created_at').filter(main_cat=main_cat_slug).filter(
                label=label_slug).filter(diff_size_only=False)
        # !=sec !=gender
        elif secondary_cat_slug != "all" and gender_slug != "all" and label_slug == 'none':
            products = product_type.objects.order_by('-created_at').filter(main_cat=main_cat_slug).filter(
                secondary_cat__secondary_cat=secondary_cat_slug).filter(gender=gender_slug).filter(diff_size_only=False)
        # !=sec != label
        elif secondary_cat_slug != "all" and gender_slug == "all" and label_slug != 'none':
            products = product_type.objects.order_by('-created_at').filter(main_cat=main_cat_slug).filter(
                secondary_cat__secondary_cat=secondary_cat_slug).filter(label=label_slug).filter(diff_size_only=False)
        # !=label !=gender
        elif secondary_cat_slug == "all" and gender_slug != "all" and label_slug != 'none':
            products = product_type.objects.order_by('-created_at').filter(main_cat=main_cat_slug).filter(
                label=label_slug).filter(gender=gender_slug).filter(diff_size_only=False)

        paginator = Paginator(products, 32)
        page = request.GET.get('page')
        paginated = paginator.get_page(page)



        context = {
            'products': paginated,
            'main_cat': main_cat_slug,
            'main_cat_list': main_cat_list,
            'secondary_cat': secondary_cat_slug,
            'secondary_cat_list': secondary_cat_list,
            'gender': gender_slug,
            'gender_list': gender_list,
            'label': label_slug,
            'label_list': label_list,
        }

        return render(request, self.template_name, context)


class ProductdetailspageView(View):
    template_name = "productspage/productdetailspage.html"
    form_class = AddProductToCart

    def get(self, request, *args, **kwargs):
        main_cat_slug = str(self.kwargs["main_cat_slug"]).lower()
        main_cat_list = MainCat.objects.order_by('-main_cat').filter(main_cat=main_cat_slug)
        secondary_cat_slug = str(self.kwargs["secondary_cat_slug"]).lower()
        gender_slug = str(self.kwargs["gender_slug"]).lower()
        product_id_slug = str(self.kwargs["product_id_slug"]).upper()
        color_slug = str(self.kwargs["color_slug"])
        if main_cat_slug == "fashion":
            product_type = ClothingProducts
            secondary_cat_list = ClothingCat.objects.order_by('-secondary_cat')
            gender_list = [('lady', '女士'), ('gentleman', '男士'), ('children', '小童'), ('others', '其他')]
        elif main_cat_slug == 'characters':
            product_type = CharacterProducts
            secondary_cat_list = CharacterCat.objects.order_by('-secondary_cat')
            gender_list = [
                ('lifestyle', '生活百貨'),
                ('accessory', '其他飾品'),
                ('stationary', '文具精品'),
                ('school', '校園生活'),
                ('workspace', '工作空間'),
            ]
        elif main_cat_slug == 'noncharacters':
            product_type = NoncharacterProducts
            secondary_cat_list = NoncharacterCat.objects.order_by('-secondary_cat')
            gender_list = [
                ('personal', '個人用品'),
                ('home', '家居'),
                ('bedroom', '睡房'),
                ('kitchen', '廚房'),
                ('washroom', '洗手間'),
                ('stationary', '文具精品'),
                ('school', '校園生活'),
                ('workspace', '工作空間'),
            ]
        label_list = [('trending', '℃ ↑'), ('sale', '%OFF'), ('preorder', '(pre-order)'), ('none', 'all')]
        products = product_type.objects.order_by('-created_at').filter(product_id=product_id_slug).filter(
            color=color_slug)
        products_color = product_type.objects.order_by('-created_at').filter(product_id=product_id_slug).filter(
            diff_size_only=False)
        product_unique = product_type.objects.order_by('-created_at').filter(product_id=product_id_slug).filter(
            color=color_slug).filter(diff_size_only=False)
        product_unique = product_unique[0]
        context = {
            'secondary_cat': secondary_cat_slug,
            'gender': gender_slug,
            'product_id': product_id_slug,
            'color': color_slug,
            'products': products,
            'products_color': products_color,
            'product_unique': product_unique,
            'main_cat': main_cat_slug,
            'secondary_cat_list': secondary_cat_list,
            'main_cat_list': main_cat_list,
            'gender_list': gender_list,
            'label_list': label_list,
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        form_product_id = form.data['product_id']
        form_color = form.data['color']
        form_size = form.data['size']
        form_quantity = form.data['quantity']
        form_selling_price = form.data['selling_price']
        form_product_id_unique = form_product_id + form_color + form_size

        if 'cart_items' not in request.session or not request.session['cart_items']:
            request.session['cart_items'] = {form_product_id_unique: [form_product_id, form_color, form_size, form_selling_price, form_quantity]}
        else:
            saved_cart = request.session['cart_items']
            saved_cart.update({form_product_id_unique: [form_product_id, form_color, form_size, form_selling_price, form_quantity]})
            request.session['cart_items'] = saved_cart

        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
