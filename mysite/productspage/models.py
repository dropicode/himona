from django.db import models
import datetime
from django.utils import timezone
from datetime import datetime
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, Transpose
import PIL
from PIL import Image
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.utils.six import StringIO
from django.urls import get_urlconf

# Create your models here.

#
class MainCat(models.Model):
    main_cat = models.CharField("Products Main Cat", blank=True, max_length=125)
    slug = models.SlugField(max_length=125, blank=True, db_index=True)
    main_cat_chi = models.CharField("Products Main Cat Chinese", blank=True, max_length=125)

    class Meta:
        ordering = ('main_cat',)
        index_together = (('id', 'slug'),)
        verbose_name = 'MainCat.'
        verbose_name_plural = 'MainCat.'

    def __str__(self):
        return self.main_cat


class ClothingCat(models.Model):
    main_cat = models.ForeignKey(MainCat, related_name='main_cat0', on_delete=models.CASCADE)
    secondary_cat = models.CharField("Products Secondary Cat", blank=True, max_length=125)
    slug = models.SlugField(max_length=125, blank=True, db_index=True)
    secondary_cat_chi = models.CharField("Products Secondary Cat Chinese", blank=True, max_length=125)

    class Meta:
        ordering = ('main_cat',)
        index_together = (('id', 'slug'),)
        verbose_name = 'ClothingCat.'
        verbose_name_plural = 'ClothingCat.'

    def __str__(self):
        return self.secondary_cat


class CharacterCat(models.Model):
    main_cat = models.ForeignKey(MainCat, related_name='main_cat1', on_delete=models.CASCADE)
    secondary_cat = models.CharField("Products Secondary Cat", blank=True, max_length=125)
    slug = models.SlugField(max_length=125, blank=True, db_index=True)
    secondary_cat_chi = models.CharField("Products Secondary Cat Chinese", blank=True, max_length=125)

    class Meta:
        ordering = ('main_cat',)
        index_together = (('id', 'slug'),)
        verbose_name = 'CharacterCat.'
        verbose_name_plural = 'CharacterCat.'

    def __str__(self):
        return self.secondary_cat


class NoncharacterCat(models.Model):
    main_cat = models.ForeignKey(MainCat, related_name='main_cat2', on_delete=models.CASCADE)
    secondary_cat = models.CharField("Products Secondary Cat", blank=True, max_length=125)
    slug = models.SlugField(max_length=125, blank=True, db_index=True)
    secondary_cat_chi = models.CharField("Products Secondary Cat Chinese", blank=True, max_length=125)

    class Meta:
        ordering = ('main_cat',)
        index_together = (('id', 'slug'),)
        verbose_name = 'NoncharacterCat.'
        verbose_name_plural = 'NoncharacterCat.'

    def __str__(self):
        return self.secondary_cat


class ClothingProducts(models.Model):
    GENDER_TYPE = (
        ('lady', 'lady'),
        ('gentleman', 'gentleman'),
        ('children', 'children'),
        ('others', 'others'),
    )
    SIZE = (
        ('N', 'NA'),
        ('S', 'small'),
        ('M', 'medium'),
        ('L', 'large'),
        ('A', 'all'),
        ('0', '0'),
        ('36', '36'),
        ('37', '37'),
    )
    LABEL_TYPE = (
        ('trending', 'trending'),
        ('sale', 'sale'),
        ('preorder', 'preorder'),
        ('none', 'none'),
    )
    product_id = models.CharField("Product ID", blank=True, max_length=125, help_text="(Updated on save)")
    product_id_unique = models.CharField("Product ID Unique", blank=True, max_length=125, help_text="(Updated on save)")
    slug = models.SlugField(max_length=125, blank=True, db_index=True)
    main_cat = models.CharField(max_length=125, default='fashion', blank=True, db_index=True, help_text="(Do not alter)")
    secondary_cat = models.ForeignKey(ClothingCat, related_name='secondary_cat0', on_delete=models.CASCADE)

    gender = models.CharField(max_length=100, choices=GENDER_TYPE, default="both", db_index=True)
    size = models.CharField(max_length=100, choices=SIZE, default="md", db_index=True)
    color = models.CharField(max_length=25, default="000000", db_index=True, help_text="(Type in a color)")

    cost = models.IntegerField(default="")
    price = models.IntegerField(default="")
    discount = models.IntegerField(default="", help_text="(Ignore %; 30% -> 30)")
    selling_price = models.IntegerField(blank=True, help_text="(Updated on save)")

    available = models.BooleanField(default=True)
    diff_size_only = models.BooleanField(default=False)
    stock = models.IntegerField(default="1")
    archives = models.BooleanField(default=False)

    description = models.TextField(default="123", blank=True)
    img1 = models.ImageField(upload_to='mysite/products/%Y%m%d', blank=True, default='mysite/blank.jpg')
    img2 = models.ImageField(upload_to='mysite/products/%Y%m%d', blank=True, default='mysite/blank.jpg')
    img3 = models.ImageField(upload_to='mysite/products/%Y%m%d', blank=True, default='mysite/blank.jpg')
    img4 = models.ImageField(upload_to='mysite/products/%Y%m%d', blank=True, default='mysite/blank.jpg')

    img1_thumbnail = ImageSpecField(source='img1',
                                    processors=[Transpose(), ResizeToFill(450, 600)],
                                    format='JPEG',
                                    options={'quality': 60})
    img2_thumbnail = ImageSpecField(source='img2',
                                    processors=[Transpose(), ResizeToFill(450, 600)],
                                    format='JPEG',
                                    options={'quality': 60})
    img3_thumbnail = ImageSpecField(source='img3',
                                    processors=[Transpose(), ResizeToFill(450, 600)],
                                    format='JPEG',
                                    options={'quality': 60})
    img4_thumbnail = ImageSpecField(source='img4',
                                    processors=[Transpose(), ResizeToFill(450, 600)],
                                    format='JPEG',
                                    options={'quality': 60})

    preorder = models.BooleanField(default=False)
    preorder_quantity = models.IntegerField(default=0)

    label = models.CharField("Label", max_length=25, choices=LABEL_TYPE, default="none", db_index=True)
    created_at = models.DateTimeField("date of creation", auto_now_add=True)
    updated_at = models.DateTimeField("last update date", auto_now=True)

    def save(self, *args, **kwargs):
        if self.product_id == "":
            self.created_at = datetime.now()
            time = datetime.strftime(self.created_at, "%y%j%H%M%S")
            name = str(self.main_cat[:3].upper() + self.secondary_cat.secondary_cat[:3].upper() + self.gender[:3] + str(hex(int(time))[2:])).upper()
            self.product_id = str(name)
        self.product_id_unique = self.product_id + self.color + self.size
        self.selling_price = self.price - self.price * self.discount/100
        if self.stock > 0:
            self.available = True
            self.preorder = False
        else:
            self.stock = 0
            self.available = False
            self.preorder = True
        super(ClothingProducts, self).save(*args, **kwargs)

    class Meta:
        ordering = ('product_id',)
        index_together = (('id', 'slug'),)
        verbose_name = 'ClothingProducts.'
        verbose_name_plural = 'ClothingProducts.'

    def __str__(self):
        return self.product_id


class CharacterProducts(models.Model):
    GENDER_TYPE = (
        ('lifestyle', 'lifestyle'),
        ('accessory', 'accessory'),
        ('stationary', 'stationary'),
        ('school', 'school'),
        ('workspace', 'workspace'),
    )
    SIZE = (
        ('N', 'NA'),
        ('S', 'small'),
        ('M', 'medium'),
        ('L', 'large'),
        ('A', 'all'),
        ('0', '0'),
        ('36', '36'),
        ('37', '37'),
    )
    LABEL_TYPE = (
        ('trending', 'trending'),
        ('sale', 'sale'),
        ('preorder', 'preorder'),
        ('none', 'none'),
    )
    product_id = models.CharField("Product ID", blank=True, max_length=125, help_text="(Updated on save)")
    product_id_unique = models.CharField("Product ID Unique", blank=True, max_length=125, help_text="(Updated on save)")
    slug = models.SlugField(max_length=125, blank=True, db_index=True)
    main_cat = models.CharField(max_length=125, default='characters', blank=True, db_index=True, help_text="(Do not alter)")
    secondary_cat = models.ForeignKey(CharacterCat, related_name='secondary_cat1', on_delete=models.CASCADE)

    gender = models.CharField(max_length=100, choices=GENDER_TYPE, default="both", db_index=True)
    size = models.CharField(max_length=100, choices=SIZE, default="md", db_index=True)
    color = models.CharField(max_length=25, default="000000", db_index=True, help_text="(Type in a color)")

    cost = models.IntegerField(default="")
    price = models.IntegerField(default="")
    discount = models.IntegerField(default="", help_text="(Ignore %; 30% -> 30)")
    selling_price = models.IntegerField(blank=True, help_text="(Updated on save)")

    available = models.BooleanField(default=True)
    diff_size_only = models.BooleanField(default=False)
    stock = models.IntegerField(default="1")
    archives = models.BooleanField(default=False)

    description = models.TextField(default="123", blank=True)
    img1 = models.ImageField(upload_to='mysite/products/%Y%m%d', blank=True, default='mysite/blank.jpg')
    img2 = models.ImageField(upload_to='mysite/products/%Y%m%d', blank=True, default='mysite/blank.jpg')
    img3 = models.ImageField(upload_to='mysite/products/%Y%m%d', blank=True, default='mysite/blank.jpg')
    img4 = models.ImageField(upload_to='mysite/products/%Y%m%d', blank=True, default='mysite/blank.jpg')

    img1_thumbnail = ImageSpecField(source='img1',
                                    processors=[Transpose(), ResizeToFill(450, 600)],
                                    format='JPEG',
                                    options={'quality': 60})
    img2_thumbnail = ImageSpecField(source='img2',
                                    processors=[Transpose(), ResizeToFill(450, 600)],
                                    format='JPEG',
                                    options={'quality': 60})
    img3_thumbnail = ImageSpecField(source='img3',
                                    processors=[Transpose(), ResizeToFill(450, 600)],
                                    format='JPEG',
                                    options={'quality': 60})
    img4_thumbnail = ImageSpecField(source='img4',
                                    processors=[Transpose(), ResizeToFill(450, 600)],
                                    format='JPEG',
                                    options={'quality': 60})

    preorder = models.BooleanField(default=False)
    preorder_quantity = models.IntegerField(default=0)

    label = models.CharField("Label", max_length=25, choices=LABEL_TYPE, default="none", db_index=True)
    created_at = models.DateTimeField("date of creation", auto_now_add=True)
    updated_at = models.DateTimeField("last update date", auto_now=True)

    def save(self, *args, **kwargs):
        if self.product_id == "":
            self.created_at = datetime.now()
            time = datetime.strftime(self.created_at, "%y%j%H%M%S")
            name = str(self.main_cat[:3].upper() + self.secondary_cat.secondary_cat[:3].upper() + self.gender[:3] + str(hex(int(time))[2:])).upper()
            self.product_id = str(name)
        self.product_id_unique = self.product_id + self.color + self.size
        self.selling_price = self.price - self.price * self.discount/100
        if self.stock > 0:
            self.available = True
            self.preorder = False
        else:
            self.stock = 0
            self.available = False
            self.preorder = True
        super(CharacterProducts, self).save(*args, **kwargs)

    class Meta:
        ordering = ('product_id',)
        index_together = (('id', 'slug'),)
        verbose_name = 'CharacterProducts.'
        verbose_name_plural = 'CharacterProducts.'

    def __str__(self):
        return self.product_id


class NoncharacterProducts(models.Model):
    GENDER_TYPE = (
        ('personal', 'personal'),
        ('home', 'home'),
        ('bedroom', 'bedroom'),
        ('kitchen', 'kitchen'),
        ('washroom', 'washroom'),
        ('stationary', 'stationary'),
        ('school', 'school'),
        ('workspace', 'workspace'),
    )
    SIZE = (
        ('N', 'NA'),
        ('S', 'small'),
        ('M', 'medium'),
        ('L', 'large'),
        ('A', 'all'),
        ('0', '0'),
        ('36', '36'),
        ('37', '37'),
    )
    LABEL_TYPE = (
        ('trending', 'trending'),
        ('sale', 'sale'),
        ('preorder', 'preorder'),
        ('none', 'none'),
    )
    product_id = models.CharField("Product ID", blank=True, max_length=125, help_text="(Updated on save)")
    product_id_unique = models.CharField("Product ID Unique", blank=True, max_length=125, help_text="(Updated on save)")
    slug = models.SlugField(max_length=125, blank=True, db_index=True)
    main_cat = models.CharField(max_length=125, default='characters', blank=True, db_index=True, help_text="(Do not alter)")
    secondary_cat = models.ForeignKey(NoncharacterCat, related_name='secondary_cat2', on_delete=models.CASCADE)

    gender = models.CharField(max_length=100, choices=GENDER_TYPE, default="both", db_index=True)
    size = models.CharField(max_length=100, choices=SIZE, default="md", db_index=True)
    color = models.CharField(max_length=25, default="000000", db_index=True, help_text="(Type in a color)")

    cost = models.IntegerField(default="")
    price = models.IntegerField(default="")
    discount = models.IntegerField(default="", help_text="(Ignore %; 30% -> 30)")
    selling_price = models.IntegerField(blank=True, help_text="(Updated on save)")

    available = models.BooleanField(default=True)
    diff_size_only = models.BooleanField(default=False)
    stock = models.IntegerField(default="1")
    archives = models.BooleanField(default=False)

    description = models.TextField(default="123", blank=True)
    img1 = models.ImageField(upload_to='mysite/products/%Y%m%d', blank=True, default='mysite/blank.jpg')
    img2 = models.ImageField(upload_to='mysite/products/%Y%m%d', blank=True, default='mysite/blank.jpg')
    img3 = models.ImageField(upload_to='mysite/products/%Y%m%d', blank=True, default='mysite/blank.jpg')
    img4 = models.ImageField(upload_to='mysite/products/%Y%m%d', blank=True, default='mysite/blank.jpg')

    img1_thumbnail = ImageSpecField(source='img1',
                                    processors=[Transpose(), ResizeToFill(450, 600)],
                                    format='JPEG',
                                    options={'quality': 60})
    img2_thumbnail = ImageSpecField(source='img2',
                                    processors=[Transpose(), ResizeToFill(450, 600)],
                                    format='JPEG',
                                    options={'quality': 60})
    img3_thumbnail = ImageSpecField(source='img3',
                                    processors=[Transpose(), ResizeToFill(450, 600)],
                                    format='JPEG',
                                    options={'quality': 60})
    img4_thumbnail = ImageSpecField(source='img4',
                                    processors=[Transpose(), ResizeToFill(450, 600)],
                                    format='JPEG',
                                    options={'quality': 60})

    preorder = models.BooleanField(default=False)
    preorder_quantity = models.IntegerField(default=0)

    label = models.CharField("Label", max_length=25, choices=LABEL_TYPE, default="none", db_index=True)
    created_at = models.DateTimeField("date of creation", auto_now_add=True)
    updated_at = models.DateTimeField("last update date", auto_now=True)

    def save(self, *args, **kwargs):
        if self.product_id == "":
            self.created_at = datetime.now()
            time = datetime.strftime(self.created_at, "%y%j%H%M%S")
            name = str(self.main_cat[:3].upper() + self.secondary_cat.secondary_cat[:3].upper() + self.gender[:3] + str(hex(int(time))[2:])).upper()
            self.product_id = str(name)
        self.product_id_unique = self.product_id + self.color + self.size
        self.selling_price = self.price - self.price * self.discount/100
        if self.stock > 0:
            self.available = True
            self.preorder = False
        else:
            self.stock = 0
            self.available = False
            self.preorder = True
        super(NoncharacterProducts, self).save(*args, **kwargs)

    class Meta:
        ordering = ('product_id',)
        index_together = (('id', 'slug'),)
        verbose_name = 'NoncharacterProducts.'
        verbose_name_plural = 'NoncharacterProducts.'

    def __str__(self):
        return self.product_id