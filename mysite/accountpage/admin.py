from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Messages


class MessagesAdmin(admin.ModelAdmin):
    list_display = ['inquiry_ref', 'username', 'created_at', 'reply']
    list_editable = ['reply']
    search_fields = ['inquiry_ref', 'username']
    list_filter = ['inquiry_ref', 'username', 'reply']


admin.site.register(Messages, MessagesAdmin)
