from django.forms import ModelForm
import sys
sys.path.append('../')
from cartpage.models import Invoice, InvoiceDetails, PromoCode
from .models import Messages
from django import forms


class InvoiceAdviceSlip(ModelForm):
    class Meta:
        model = Invoice
        fields = [
            'advice_slip_file',
            'invoice',
            'advice_slip',
            'delivery_status',
        ]


class Remarks(ModelForm):
    class Meta:
        model = Invoice
        fields = [
            'invoice',
            'remarks',
            'remarks_unread',
        ]


class OutstandingDetailForm(ModelForm):
    class Meta:
        model = Invoice
        fields = [
            'invoice',
            'payment_method',
            'delivery_method',
            'address',
            'cancel',
        ]


class PromoCodeApplyForm(ModelForm):
    class Meta:
        model = PromoCode
        fields = [
            'promo_code',
        ]


class AddMessageForm(ModelForm):
    class Meta:
        model = Messages
        fields = [
            'username',
            'title',
            'goal',
            'message',
        ]