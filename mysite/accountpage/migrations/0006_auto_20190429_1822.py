# Generated by Django 2.2 on 2019-04-29 09:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accountpage', '0005_auto_20190429_1819'),
    ]

    operations = [
        migrations.AlterField(
            model_name='messages',
            name='goal',
            field=models.CharField(choices=[('surrogate', 'surrogate shopping'), ('product_info', 'about our products'), ('exchange', 'items exchange'), ('refund', 'refund'), ('cancel', 'cancel'), ('return', 'returning items'), ('complaint', 'complaint'), ('other', 'other inquiry'), ('reply', 'reply')], db_index=True, default='inquiry', max_length=100),
        ),
    ]
