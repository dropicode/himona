"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path("orderspage", views.AccountpageView.as_view(), name="orderspage"),
    path("orderspage/outstanding/pay/<slug:payment_method_slug>/<slug:invoice_slug>", views.OrderprocessingpageView.as_view(), name="orderprocessingpage"),
    path("orderspage/outstanding/view/<slug:invoice_slug>", views.OutstandingpageView.as_view(), name="outstandingpage"),
    path("orderspage/paid/view/<slug:invoice_slug>", views.PaidpageView.as_view(), name="paidpage"),
    path("orderspage/paid/remarks/<slug:invoice_slug>", views.RemarkspageView.as_view(), name="remarkspage"),
    path("orderspage/complete/view/<slug:invoice_slug>", views.CompletepageView.as_view(), name="completepage"),
    path("orderspage/outstanding/promo/<slug:invoice_slug>", views.PromocodepageView.as_view(), name="promocodepage"),
    path("orderspage/outstanding/stripeaccepted/<slug:invoice_slug>", views.StripereturnpageView.as_view(), name="stripereturnpage"),
    path("orderspage/outstanding/pay/invalid", views.InvalidpageView.as_view(), name="invalidpage"),
    path("messengerpage", views.MessengerpageView.as_view(), name="messengerpage"),
    path("addmessage", views.Addmessagepage.as_view(), name="addmessagepage"),
    #path("messengerpage/title/<slug:inquiry_ref_slug>", views.MessengertitlepageView.as_view(), name="messengertitlepage"),
    path("messengerpage/messages/<slug:inquiry_ref_slug>", views.MessengermessagespageView.as_view(), name="messengermessagespage"),
]
