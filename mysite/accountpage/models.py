import datetime
from django.db import models
from django.utils import timezone
from datetime import datetime
from django.core.files import File
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from io import BytesIO
from django.conf import settings
from os import remove
from django.utils import timezone
from datetime import datetime

# Create your models here.


class Messages(models.Model):
    GOAL_TYPE = (
        ('comment', 'comment'),
        ('surrogate', 'surrogate shopping'),
        ('product_info', 'about our products'),
        ('exchange', 'items exchange'),
        ('refund', 'refund'),
        ('cancel', 'cancel'),
        ('return', 'returning items'),
        ('complaint', 'complaint'),
        ('other', 'other inquiry'),
        ('reply', 'reply'),
    )

    username = models.CharField(default="", max_length=300)
    inquiry_ref = models.CharField(default="", max_length=400, db_index=True, blank=True)
    slug = models.SlugField(max_length=300, blank=True, db_index=True)
    title = models.CharField(default="", max_length=300, help_text="300 characters less", db_index=True)
    title_check = models.BooleanField(default=False)
    goal = models.CharField(max_length=100, choices=GOAL_TYPE, default="inquiry", db_index=True)
    message = models.TextField(default="", max_length="2000", help_text="2000 characters less")
    user_created = models.BooleanField(default=True)
    reply = models.BooleanField(default=False)
    close = models.BooleanField(default=False)
    created_at = models.DateTimeField("date of creation", auto_now_add=True)

    class Meta:
        ordering = ('username',)
        index_together = (('id', 'slug'),)
        verbose_name = 'messages'
        verbose_name_plural = 'messages'

    def __str__(self):
        return self.username

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

    def save(self, *args, **kwargs):
        if self.user_created == True:
            if self.inquiry_ref == "":
                self.created_at = datetime.now()
                time = datetime.strftime(self.created_at, "%y%j%H%M%S")
                name = self.username
                self.inquiry_ref = name + time
                self.created_at = datetime.now()

            if self.title_check == False:
                title = self.title
                self.title = title + " ' " + time + " ' "
                self.title_check = True


            super(Messages, self).save(*args, **kwargs)

