from django.views import View
from django.urls import reverse
from paypal.standard.forms import PayPalPaymentsForm
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.conf import settings
import sys
sys.path.append('../')
from productspage.models import ClothingProducts, CharacterProducts, NoncharacterProducts
from cartpage.models import Invoice, InvoiceDetails, PromoCode
from django.views.decorators.csrf import csrf_exempt
from hashlib import md5
from . import forms
from .models import Messages
from datetime import date, timedelta, datetime
import pytz
import stripe
from django.contrib import messages
from itertools import chain
from operator import attrgetter
from django.core.paginator import Paginator
import requests
from django.db.models import Q
from django.contrib.auth import login, logout, authenticate
stripe.api_key = settings.STRIPE_SECRET_KEY


class AccountpageView(View):
    # displayorder history
    template_name = "accountpage/orderspage.html"

    def get(self, request, *args, **kwargs):

        username = request.session['username']
        outstanding_invoice = Invoice.objects.order_by('-created_at').filter(outstanding=True).filter(username=username)
        paid_invoice = Invoice.objects.order_by('-created_at').filter(outstanding=False).filter(complete=False).filter(username=username)
        completed_invoice = Invoice.objects.order_by('-created_at').filter(outstanding=False).filter(complete=True).filter(username=username)[:25]

        for i in outstanding_invoice:
            p1 = i
            date_format = "%d/%m/%Y"
            utc = pytz.UTC
            date_limit1 = datetime.now() - timedelta(hours=48)
            date_limit = utc.localize(date_limit1)
            if i.created_at < date_limit:
                article = Invoice.objects.filter(invoice=i.invoice).get()
                if article.cancel == True:
                    pass
                else:
                    invoice_detail_item = InvoiceDetails.objects.order_by('product_id_unique').filter(
                        details_invoice__invoice__contains=i.invoice)
                    for j in invoice_detail_item:
                        if j.product_id_unique[0:3] == "FAS":
                            Products = ClothingProducts
                        elif j.product_id_unique[0:3] == 'CHA':
                            Products = CharacterProducts
                        elif j.product_id_unique[0:3] == 'NON':
                            Products = NoncharacterProducts
                        product_details = Products.objects.filter(product_id_unique=j.product_id_unique)[0]
                        if j.preorder == False:
                            product_details.stock = product_details.stock + j.quantity
                        else:
                            product_details.preorder_quantity = product_details.preorder_quantity - j.quantity
                        product_details.save()
                    article.cancel = True
                    article.save()
            else:
                pass

        context = {
            'outstanding_invoice': outstanding_invoice,
            'paid_invoice': paid_invoice,
            'completed_invoice': completed_invoice,
        }

        return render(request, self.template_name, context)


class OrderprocessingpageView(View):
    template_name = "accountpage/orderprocessing.html"
    form_class = forms.InvoiceAdviceSlip

    def email_sending(self, request, invoice_id, total_amount):
        from django.template.loader import render_to_string
        from django.contrib.auth.models import User
        from django.core.mail import send_mail
        username = request.session['username']
        user = User.objects.filter(username=username).get()

        subject = str("HIMONA: Payment received")
        message = render_to_string('accountpage/receiptemail.html', {
            'user': user,
            'invoice_id': invoice_id,
            'total_amount': total_amount,
        })

        email_from = settings.EMAIL_HOST_USER
        user_email = str(user.email)
        recipient_list = [user_email, ]
        send_mail(subject, message, email_from, recipient_list, auth_user=settings.EMAIL_HOST_USER,
                  auth_password=settings.EMAIL_HOST_PASSWORD)

    @csrf_exempt
    def get(self, request, *args, **kwargs):
        invoice_slug = str(self.kwargs['invoice_slug'])
        payment_method_slug = str(self.kwargs['payment_method_slug'])

        #create paypal button
        '''
        if payment_method_slug == "P":
            invoice_item = Invoice.objects.filter(invoice=invoice_slug).get()

            m = md5()
            paypal_invoice = "salt" + invoice_slug
            m.update(paypal_invoice.encode())
            request.session['paypal_invoice'] = str((m.hexdigest()))
            request.session['current_invoice'] = invoice_slug

            paypal_dict = {
                "business": "ny48uk-facilitator-1@gmail.com",
                "amount": str(invoice_item.total_amount),
                "item_name": str(invoice_slug),
                "invoice": str(invoice_slug),
                "currency_code": "HKD",
                "notify_url": request.build_absolute_uri(reverse('paypal-ipn')),
                #change return url to a new view that picks up session and renewing the order status
                "return": request.build_absolute_uri(reverse('myshop_myaccount_paypal_success', kwargs={'invoice_slug': str((m.hexdigest()))})),
                "cancel_return": request.build_absolute_uri(reverse('myshop_myaccount_paypal_canceled')),
                "custom": "",  # Custom command to correlate to some function later (optional)
            }
            # Create the instance.
            form = PayPalPaymentsForm(initial=paypal_dict)

            context = {
                'form': form,
                'invoice_slug': invoice_slug,
                'total_amount': str(invoice_item.total_amount),
            }
            return render(request, self.template_name, context)
        '''


        if payment_method_slug == "O":
            '''
            invoice_item = Invoice.objects.filter(invoice=invoice_slug).get()
            total_amount = invoice_item.total_amount
            template_name = "accountpage/orderprocessingstripe.html"

            context = {
                'total_amount': total_amount*100,
                'total_amount_content':total_amount,
                'pub_key': settings.STRIPE_PUBLISHABLE_KEY,
                'invoice_id': invoice_slug,
            }
            '''
            m = md5()
            stripe_invoice = "salt" + invoice_slug
            m.update(stripe_invoice.encode())
            request.session['stripe_invoice'] = str((m.hexdigest()))
            request.session['current_invoice'] = invoice_slug

            template_name = "accountpage/orderprocessingstripe.html"
            invoice_item = Invoice.objects.filter(invoice=invoice_slug).get()
            total_amount = invoice_item.total_amount
            stripe.api_key = "sk_test_qDJc8RXexxopiUJmlPEEF79P003SLY2w5F"
            payment = stripe.checkout.Session.create(
                cancel_url=request.build_absolute_uri(reverse('invalidpage')),
                success_url=request.build_absolute_uri(reverse('stripereturnpage', kwargs={'invoice_slug': str((m.hexdigest()))})),
                payment_method_types=['card'],
                line_items=[
                    {
                        'amount': total_amount*100,
                        'currency': 'hkd',
                        'name': invoice_slug,
                        'description': 'Your Order:',
                        'quantity': 1,
                    }
                ]
            )
            context = {
                'payment_id': payment.id,
                'total_amount': total_amount * 100,
                'total_amount_content': total_amount,
                'pub_key': settings.STRIPE_PUBLISHABLE_KEY,
                'invoice_id': invoice_slug,
                'invoice_slug': invoice_slug,
            }
            return render(request, template_name, context)

        # create upload button
        else:
            template_name = "accountpage/orderprocessingadviceslip.html"
            #article = Invoice.objects.filter(invoice=invoice_slug).get()
            form = forms.InvoiceAdviceSlip()
            invoice_item = Invoice.objects.filter(invoice=invoice_slug).get()

            context = {
                'form': form,
                'invoice_slug': invoice_slug,
                'total_amount': str(invoice_item.total_amount),
                'error': '',
            }

            return render(request, template_name, context)

    def post(self, request, *args, **kwargs):
        payment_method_slug = str(self.kwargs['payment_method_slug'])
        invoice_slug = str(self.kwargs['invoice_slug'])

        if payment_method_slug == 'O':
            '''
            p1 = Invoice.objects.filter(invoice=invoice_slug).get()
             try:
                 charge = stripe.Charge.create(
                     amount=p1.total_amount*100,
                     currency='hkd',
                     description=invoice_slug,
                     source=request.POST['stripeToken']
                 )
                 p1.outstanding = False
                 p1.advice_slip = True
                 p1.delivery_status = "P"
                 p1.save()
                 self.email_sending(request, invoice_slug, p1.total_amount)
                 return HttpResponseRedirect(reverse('orderspage'))
 
             except (
                     stripe.error.CardError,
                     stripe.error.RateLimitError,
                     stripe.error.InvalidRequestError,
                     stripe.error.AuthenticationError,
                     stripe.error.APIConnectionError,
                     stripe.error.StripeError
             ):
                 template_name = "accountpage/invalid.html"
                 # article = Invoice.objects.filter(invoice=invoice_slug).get()
                 form = forms.InvoiceAdviceSlip()
                 invoice_item = Invoice.objects.filter(invoice=invoice_slug).get()
 
                 context = {
                     'form': form,
                     'invoice_slug': invoice_slug,
                     'total_amount': str(invoice_item.total_amount),
                     'error': 'Card Invalid, Please try again',
                 }
                 return render(request, template_name, context)
            '''

        else:
            #posting advice slip to db
            invoice_slug = str(self.kwargs['invoice_slug'])
            article = Invoice.objects.filter(invoice=invoice_slug).get()
            form = forms.InvoiceAdviceSlip(request.POST, request.FILES, instance=article)
            form.save()
            messages.success(request, 'Uploaded')
            return HttpResponseRedirect(reverse('orderspage'))


class StripereturnpageView(View):
    def get(self, request, *args, **kwargs):
        #check validation
        current_invoice = str(self.kwargs['invoice_slug'])
        m = md5()
        stripe_invoice_session = request.session['current_invoice']
        stripe_invoice = "salt" + stripe_invoice_session
        m.update(stripe_invoice.encode())
        paypal_invoice = str((m.hexdigest()))
        if paypal_invoice == current_invoice:
            p1 = Invoice.objects.filter(invoice=stripe_invoice_session).get()
            total_amount = p1.total_amount
            invoice_id = p1.invoice
            p1.outstanding = False
            p1.advice_slip = True
            p1.delivery_status = "P"
            p1.save()
            try:
                del request.session['current_invoice']
                del request.session['stripe_invoice']
            except KeyError:
                pass
            OrderprocessingpageView.email_sending(self, request, invoice_id, total_amount)
            return HttpResponseRedirect(reverse('orderspage'))
        else:
            return HttpResponseRedirect(reverse('invalidpage'))


class InvalidpageView(View):
    template_name = "accountpage/invalid.html"
    def get(self, request, *args, **kwargs):
        try:
            del request.session['current_invoice']
            del request.session['stripe_invoice']
        except KeyError:
            pass
        return render(request, self.template_name)


class OutstandingpageView(View):
    template_name = "accountpage/outstanding.html"
    form_class = forms.OutstandingDetailForm

    def get(self, request, *args, **kwargs):
        invoice_slug = str(self.kwargs['invoice_slug'])
        invoice_item = Invoice.objects.filter(invoice=invoice_slug).get()
        invoice_detail_item = InvoiceDetails.objects.order_by('product_id_unique').filter(details_invoice__invoice__contains=invoice_slug)

        count = 1
        for i in invoice_detail_item:
            if i.product_id_unique[0:3] == "FAS":
                Products = ClothingProducts
            elif i.product_id_unique[0:3] == 'CHA':
                Products = CharacterProducts
            elif i.product_id_unique[0:3] == 'NON':
                Products = NoncharacterProducts
            if count == 1:
                product_details = Products.objects.filter(product_id_unique=i.product_id_unique)
                count = count + 1
            else:
                if product_details[(count-2)].product_id_unique == i.product_id_unique:
                    pass
                else:
                    current_product_details = Products.objects.filter(product_id_unique=i.product_id_unique)
                    product_details = sorted(chain(product_details, current_product_details), key=attrgetter('available'))
                    #, key=attrgetter('created_at')
                    count = count + 1


        form = self.form_class(instance=invoice_item)
        context = {
            'invoice_detail_item': invoice_detail_item,
            'product_details': product_details,
            'form': form,
            'invoice_slug': invoice_slug,
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        #posting advice slip to db
        invoice_slug = str(self.kwargs['invoice_slug'])
        article = Invoice.objects.filter(invoice=invoice_slug).get()
        form = self.form_class(request.POST or None, instance=article)
        form.save()
        data = form.cleaned_data

        #if data['cancel'] is True:
        #   article.delete()
        #   print("YES")

        return HttpResponseRedirect(reverse('orderspage'))


class PaidpageView(View):
    template_name = "accountpage/paid.html"

    def get(self, request, *args, **kwargs):
        invoice_slug = str(self.kwargs['invoice_slug'])
        invoice_item = Invoice.objects.filter(invoice=invoice_slug).get()
        invoice_detail_item = InvoiceDetails.objects.filter(details_invoice__invoice__contains=invoice_slug)
        count = 1
        for i in invoice_detail_item:
            if i.product_id_unique[0:3] == "FAS":
                Products = ClothingProducts
            elif i.product_id_unique[0:3] == 'CHA':
                Products = CharacterProducts
            elif i.product_id_unique[0:3] == 'NON':
                Products = NoncharacterProducts
            if count == 1:
                product_details = Products.objects.filter(product_id_unique=i.product_id_unique)
                count = count + 1
            else:
                if product_details[(count - 2)].product_id_unique == i.product_id_unique:
                    pass
                else:
                    current_product_details = Products.objects.filter(product_id_unique=i.product_id_unique)
                    product_details = sorted(chain(product_details, current_product_details),
                                             key=attrgetter('created_at'))
                    count = count + 1

        context = {
            'invoice_detail_item': invoice_detail_item,
            'product_details': product_details,
            'invoice_slug': invoice_slug,
            'invoice_item': invoice_item,
        }
        return render(request, self.template_name, context)


class RemarkspageView(View):
    template_name = "accountpage/remarks.html"
    form_class = forms.Remarks
    def get(self, request, *args, **kwargs):
        invoice_slug = str(self.kwargs['invoice_slug'])
        form = self.form_class()
        invoice_item = Invoice.objects.filter(invoice=invoice_slug).get()
        context = {
            'form': form,
            'invoice_slug': invoice_slug,
            'invoice_item': invoice_item,
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        #posting advice slip to db
        invoice_slug = str(self.kwargs['invoice_slug'])
        article = Invoice.objects.filter(invoice=invoice_slug).get()
        form = self.form_class(request.POST)
        if article.delivery_method == 'DL' and article.delivery_status != 'P':
            error_msg = "Your order has already been dispatched."
            return HttpResponseRedirect(reverse('invalidpage'))
        else:
            article.remarks = form.data['remarks']
            article.remarks_unread = True
            article.save()
            return HttpResponseRedirect(reverse('orderspage'))


class CompletepageView(View):
    template_name = "accountpage/complete.html"

    def get(self, request, *args, **kwargs):
        invoice_slug = str(self.kwargs['invoice_slug'])
        invoice_item = Invoice.objects.filter(invoice=invoice_slug).get()
        invoice_detail_item = InvoiceDetails.objects.filter(details_invoice__invoice__contains=invoice_slug)
        count = 1
        for i in invoice_detail_item:
            if i.product_id_unique[0:3] == "FAS":
                Products = ClothingProducts
            elif i.product_id_unique[0:3] == 'CHA':
                Products = CharacterProducts
            elif i.product_id_unique[0:3] == 'NON':
                Products = NoncharacterProducts
            if count == 1:
                product_details = Products.objects.filter(product_id_unique=i.product_id_unique)
                count = count + 1
            else:
                if product_details[(count - 2)].product_id_unique == i.product_id_unique:
                    pass
                else:
                    current_product_details = Products.objects.filter(product_id_unique=i.product_id_unique)
                    product_details = sorted(chain(product_details, current_product_details),
                                             key=attrgetter('created_at'))
                    count = count + 1

        context = {
            'invoice_detail_item': invoice_detail_item,
            'product_details': product_details,
            'invoice_slug': invoice_slug,
        }
        return render(request, self.template_name, context)


class PromocodepageView(View):
    template_name = "accountpage/promocode.html"
    form_class = forms.PromoCodeApplyForm

    def get(self, request, *args, **kwargs):
        invoice_slug = str(self.kwargs['invoice_slug'])
        invoice_item = Invoice.objects.filter(invoice=invoice_slug).get()
        all_promo_code = PromoCode.objects.order_by('-created_at')
        form = self.form_class()
        context = {
            'form': form,
            'invoice_item': invoice_item,
            'invoice_slug': invoice_slug,
            'all_promo_code': all_promo_code,
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        #posting advice slip to db
        invoice_slug = str(self.kwargs['invoice_slug'])
        article = Invoice.objects.filter(invoice=invoice_slug).get()
        form = self.form_class(request.POST)
        #form.save()
        #data = form.cleaned_data
        all_promo_code = PromoCode.objects.order_by('-created_at').filter(validity=True)
        check = False
        if len(all_promo_code) != 0:
            for i in all_promo_code:
                if i.promo_code == form.data['promo_code']:
                    check = True
                    article.ori_total_amount = article.total_amount
                    article.total_amount = int(round(article.total_amount*(100-i.percent_off)/100))
                    article.promo = True
                    article.promo_code = form.data['promo_code']
                    article.save()
                    promo_i = PromoCode.objects.filter(promo_code=form.data['promo_code']).get()

                    if promo_i.one_off == True:
                        promo_i.validity = False
                        promo_i.save()
                    break
                else:
                    pass

        if check == True:
            return HttpResponseRedirect(reverse('orderspage'))
        else:
            return HttpResponseRedirect(reverse('invalidpage'))


class MessengerpageView(View):
    template_name = "accountpage/messengerpage.html"

    def get(self, request, *args, **kwargs):
        username = request.session['username']
        try:
            user_messages = Messages.objects.order_by('-created_at').filter(Q(username=username) | Q(username='ny48uk')).filter(close=False)
            user_titles = []
            count = 1
            for i in user_messages:
                title = str(i.title)
                if count == 1:
                    tup = tuple((i.inquiry_ref, title))
                    user_titles.append(tup)
                    count = count + 1
                else:
                    check = title in user_titles
                    if check == False:
                        tup = tuple((i.inquiry_ref, title))
                        user_titles.append(tup)
                    else:
                        pass

            paginator = Paginator(user_messages, 24)
            page = request.GET.get('page')
            paginated_user_messages = paginator.get_page(page)

            context = {
                'user_messages': paginated_user_messages,
                'user_titles': user_titles,
                'username': username,
            }
        except AttributeError:
            context = {

            }

        return render(request, self.template_name, context)


class Addmessagepage(View):
    template_name = "accountpage/addmessagepage.html"
    form_class = forms.AddMessageForm

    def email_sending(self, request, inquiry_ref, title, message_content):
        from django.template.loader import render_to_string
        from django.contrib.auth.models import User
        from django.core.mail import send_mail
        username = request.session['username']
        user = User.objects.filter(username=username).get()

        subject = str("HIMONA: We have received your message--ref: " + str(inquiry_ref))
        message = render_to_string('accountpage/messageemail.html', {
            'user': user,
            'inquiry_ref': inquiry_ref,
            'title': title,
            'message_content': message_content,
        })

        email_from = settings.EMAIL_HOST_USER
        user_email = str(user.email)
        recipient_list = [user_email, "ny48uk@gmail.com"]
        send_mail(subject, message, email_from, recipient_list, auth_user=settings.EMAIL_HOST_USER,
                  auth_password=settings.EMAIL_HOST_PASSWORD)

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        try:
            username = request.session['username']
        except KeyError:
            return HttpResponseRedirect(reverse('signinpage'))
        goal_list = [
            ('comment', '留言'),
            ('surrogate', '代購資訊'),
            ('product_info', '貨品資訊'),
            ('exchange', '更換貨品'),
            ('refund', '退款查詢'),
            ('cancel', '取消查詢'),
            ('return', '退貨查詢'),
            ('complaint', '投訴'),
            ('other', '其他查詢'),
        ]
        goal_list_eng = [
            ('comment', 'comment'),
            ('surrogate', 'surrogate shopping'),
            ('product_info', 'about our products'),
            ('exchange', 'items exchange'),
            ('refund', 'refund'),
            ('cancel', 'cancel'),
            ('return', 'returning items'),
            ('complaint', 'complaint'),
            ('other', 'other inquiry'),
        ]

        context = {
            'form': form,
            'goal_list': goal_list,
            'goal_list_eng': goal_list_eng,
            'username': username,
        }

        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        username = request.session['username']
        if form.is_valid():
            recaptcha_response = request.POST.get('g-recaptcha-response')
            data = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
            }
            r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
            result = r.json()
            # End reCAPTCHA validation
            if result['success']:
                message = form.save(commit=False)
                message.save()
                user_message = Messages.objects.order_by('-created_at').filter(username=username).filter(close=False)[0]
                inquiry_ref = user_message.inquiry_ref
                title = user_message.title
                message_content = user_message.message
                Addmessagepage.email_sending(self, request, inquiry_ref, title, message_content)

                return HttpResponseRedirect(reverse('messengerpage'))

            else:
                context = {
                    'form': form,
                }
                messages.error(request, 'Invalid reCAPTCHA. Please try again.')
                return render(request, self.template_name, context)


class MessengermessagespageView(View):
    template_name = "accountpage/messengermessagespage.html"
    form_class = forms.AddMessageForm

    def get(self, request, *args, **kwargs):
        inquiry_ref_slug = str(self.kwargs["inquiry_ref_slug"])

        form = self.form_class()
        username = request.session['username']
        message_unique = Messages.objects.order_by('-created_at').filter(inquiry_ref=inquiry_ref_slug).filter(close=False)
        messages = Messages.objects.order_by('-created_at').filter(title=message_unique[0].title).filter(close=False)
        context = {
            'form': form,
            'username': username,
            'inquiry_ref': inquiry_ref_slug,
            'message_unique': message_unique,
            'messages': messages,
        }

        return render(request, self.template_name, context)

