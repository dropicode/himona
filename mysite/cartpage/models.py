import datetime, os
from django.db import models
from django.utils import timezone
from datetime import datetime
from django.core.files import File
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from io import BytesIO
from django.conf import settings
from os import remove


class Invoice(models.Model):

    PAYMENT_METHOD = (
        ('O', 'CreditCards'),
        ('T', 'BankTransfer'),
    )

    DELIVERY_METHOD = (
        ('PU', 'PickUpAtStore'),
        ('DL', 'Delivery'),
    )

    DELIVERY_STATUS = (
        ('W', 'Waiting for the payment'),
        ('A', 'upload received'),
        ('P', 'Preparing'),
        ('D', 'Dispatched'),
        ('R', 'Ready to collect'),
        ('C', 'Collected')
    )

    invoice = models.CharField(max_length=250, blank=True, help_text="(updated on save)", db_index=True)
    slug = models.SlugField(max_length=100, blank=True, db_index=True)
    username = models.CharField(max_length=125, blank=False, help_text="(updated on save)")
    first_name = models.CharField(max_length=125, blank=False, help_text="Receiver's first name")
    last_name = models.CharField(max_length=125, blank=True, help_text="Receiver's last name")
    payment_method = models.CharField(max_length=100, choices=PAYMENT_METHOD, default="both", db_index=True)
    delivery_method = models.CharField(max_length=100, choices=DELIVERY_METHOD, default="both", db_index=True)
    total_amount = models.IntegerField(blank=True, help_text="(updated on save)")
    total_cost = models.IntegerField(blank=True, help_text="(updated on save)")
    phone_no = models.CharField(max_length=100, blank=True, help_text="(updated on save)")
    address = models.TextField(default="Pick up at store")
    remarks = models.CharField(max_length=125, blank=True, default="")
    remarks_unread = models.BooleanField(default=False)
    promo = models.BooleanField(default=False)
    promo_code = models.CharField(max_length=125, blank=True, help_text="(updated on save)")
    ori_total_amount = models.IntegerField(default="", blank=True)

    outstanding = models.BooleanField(default=True)
    delivery_status = models.CharField(max_length=100, choices=DELIVERY_STATUS, default="P")
    advice_slip = models.BooleanField(default=False)
    advice_slip_file = models.ImageField(upload_to='myshop/invoice/%Y%m%d', blank=True,)
    ready = models.BooleanField(default=False)
    complete = models.BooleanField(default=False)
    cancel = models.BooleanField(default=False)
    refund = models.BooleanField(default=False)

    created_at = models.DateTimeField("date of creation", auto_now_add=True)
    eta = models.CharField(max_length=50, blank=True, default="")
    check_outstanding = models.BooleanField(default=False)
    invoice_file = models.FileField(upload_to='invoices/%Y%m%d', blank=True)
    printed = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.invoice == "":
            self.created_at = datetime.now()
            time = datetime.strftime(self.created_at, "%Y%m%d%H%M%S")
            self.invoice = self.username + str(time) + self.payment_method + str(self.total_amount) + self.first_name[0] + self.last_name[0]
            self.slug = self.invoice
            super(Invoice, self).save(*args, **kwargs)
        else:
            if self.outstanding == False and self.check_outstanding == False:
                self.check_outstanding = True
                time= datetime.now()
                time_invoice = datetime.strftime(time, "%d/%m/%Y @%H:%M")
                #595*842
                canvas1 = canvas.Canvas(os.path.join(settings.MEDIA_ROOT, str(self.invoice + ".pdf")))
                canvas1.setLineWidth(.3)
                canvas1.setFont('Helvetica', 18)
                canvas1.drawCentredString(298, 760, "OFFICIAL INNVOICE OF HIMONA")
                canvas1.line(30, 740, 565, 740)

                canvas1.setFont('Helvetica', 12)
                canvas1.drawString(30, 710, "INVOICE No.: " + str(self.invoice))
                canvas1.drawString(380, 710, "PAYMENT VIA: " + str(self.payment_method))
                canvas1.drawString(30, 680, "ORDERED BY: " + str(self.username))
                canvas1.drawString(380, 680, "DELIVERY TO: " + str(self.delivery_method))

                canvas1.drawString(30, 650, "PRODUCT ID")
                canvas1.drawString(230, 650, "SELLING PRICE/UNIT $")
                canvas1.drawString(380, 650, "QUANTITY")
                canvas1.drawString(460, 650, "TOTAL PRICE ($)")
                canvas1.drawString(30, 620, "ADDRESS: " + str(self.address))
                canvas1.line(30, 610, 565, 610)
                canvas1.line(30, 607, 565, 607)

                height = 620
                line = 610
                interval = 40

                items = InvoiceDetails.objects.filter(details_invoice__invoice=self.invoice)
                canvas1.setFont('Helvetica', 12)
                counter_page = 7
                page = 1
                canvas1.drawCentredString(298, 30, str("Page" + str(page)))
                for i in items:
                    stop = False
                    counter_page = counter_page - 1
                    if counter_page == -1:
                        page = page + 1
                        canvas1.showPage()
                        canvas1.drawCentredString(298, 760, "OFFICIAL INNVOICE OF HIMONA")
                        canvas1.line(30, 740, 565, 740)

                        canvas1.setFont('Helvetica', 12)
                        canvas1.drawString(30, 710, "INVOICE No.: " + str(self.invoice))
                        canvas1.drawString(380, 710, "PAYMENT VIA: " + str(self.payment_method))
                        canvas1.drawString(30, 680, "ORDERED BY: " + str(self.username))
                        canvas1.drawString(380, 680, "DELIVERY TO: " + str(self.delivery_method))

                        canvas1.drawString(30, 650, "PRODUCT ID")
                        canvas1.drawString(230, 650, "SELLING PRICE/UNIT $")
                        canvas1.drawString(380, 650, "QUANTITY")
                        canvas1.drawString(460, 650, "TOTAL PRICE ($)")
                        canvas1.drawString(30, 620, "ADDRESS: " + str(self.address))
                        canvas1.line(30, 610, 565, 610)
                        canvas1.line(30, 607, 565, 607)

                        height = 620
                        line = 610
                        interval = 40

                        canvas1.setFont('Helvetica', 12)
                        counter_page = 7
                        stop = True
                        canvas1.drawCentredString(298, 30, str("Page" + str(page)))
                        canvas1.drawString(30, (height - interval), i.product_id_unique)
                        canvas1.drawString(230, (height - interval), str(i.selling_price))
                        canvas1.drawString(380, (height - interval), str(i.quantity))
                        canvas1.drawString(460, (height - interval), str(i.selling_price * i.quantity))
                        if i.preorder == True:
                            canvas1.drawString(530, (height - interval), str("Pre"))

                        height = height - interval
                        line = line - interval
                    else:
                        stop = True
                        canvas1.drawString(30, (height - interval), i.product_id_unique)
                        canvas1.drawString(230, (height - interval), str(i.selling_price))
                        canvas1.drawString(380, (height - interval), str(i.quantity))
                        canvas1.drawString(460, (height - interval), str(i.selling_price * i.quantity))
                        if i.preorder == True:
                            canvas1.drawString(530, (height - interval), str("Pre"))

                        height = height - interval
                        line = line - interval


                canvas1.drawCentredString(298, (line-5), "--------------------------------------------------------------------------------------------------------------------------------------")
                #canvas1.line(30, (line), 565, (line))
                canvas1.drawString(380, (height - interval), "TOTAL $: ")
                canvas1.drawString(460, (height - interval), str(self.total_amount))
                canvas1.drawString(30, (height - interval), str("SETTLED ON: " + time_invoice))
                height = height - interval - interval
                line = line - interval - interval
                canvas1.drawString(30, (height - interval), str("RECEIVED BY: ______________________"))
                canvas1.drawString(380, (height - interval), str("RECEIVED ON: ______________"))
                height = height - interval
                line = line - interval
                canvas1.drawString(30, (height - interval), str("(NAME & SIGNATURE)"))
                height = height - interval
                line = line - interval
                canvas1.line(30, (line), 565, (line))
                canvas1.line(30, (line-3), 565, (line-3))
                canvas1.drawCentredString(298, (height - interval), "END OF INVOICE")
                canvas1.save()


                #file = open(str(self.invoice + ".pdf"), "rb")
                file = open(os.path.join(settings.MEDIA_ROOT, str(self.invoice + ".pdf")), 'rb+')
                self.invoice_file.save(str(self.invoice + ".pdf"), file)
                #self.invoice_file.save(os.path.join(settings.MEDIA_ROOT, str(self.invoice + ".pdf")), file)
                file.close()
                #remove(str(self.invoice + ".pdf"))
                remove(os.path.join(settings.MEDIA_ROOT, str(self.invoice + ".pdf")))
                super(Invoice, self).save(*args, **kwargs)
            else:
                super(Invoice, self).save(*args, **kwargs)

    class Meta:
        ordering = ('invoice',)
        index_together = (('id', 'slug'),)
        verbose_name = 'invoice'
        verbose_name_plural = 'invoices'

    def __str__(self):
        return self.invoice

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)


class InvoiceDetails(models.Model):
    details_invoice = models.ForeignKey(Invoice, related_name='invoice_no', blank=True, on_delete=models.CASCADE)
    #invoice = models.CharField("Invoice No.", db_index=True, max_length=250, blank=True, help_text="(updated on save)")
    username = models.CharField(max_length=125, blank=False, help_text="(updated on save)")

    product_id_unique = models.CharField(max_length=125)
    product_id = models.CharField(max_length=25, db_index=True)
    color = models.CharField(max_length=25, db_index=True)
    size = models.CharField(max_length=100, db_index=True)
    selling_price = models.IntegerField()
    quantity = models.IntegerField()
    total_cost = models.IntegerField(blank=True, help_text="(updated on save)")
    preorder = models.BooleanField(default=False)
    ready = models.BooleanField(default=False)
    dispatched = models.BooleanField(default=False)


class PromoCode(models.Model):
    promo_code = models.CharField(max_length=25, db_index=True)
    percent_off = models.IntegerField(blank=True, help_text="(updated on save)")
    validity = models.BooleanField(default=True)
    one_off = models.BooleanField(default=False)
    description = models.CharField(max_length=300, db_index=True)

    created_at = models.DateTimeField("date of creation", auto_now_add=True)

    def save(self, *args, **kwargs):
        self.created_at = datetime.now()

        super(PromoCode, self).save(*args, **kwargs)