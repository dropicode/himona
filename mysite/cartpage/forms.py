from django.forms import ModelForm
from .models import Invoice
from django import forms


class InvoiceIdGen(ModelForm):

    class Meta:
        model = Invoice
        fields = [
            'invoice',
            'username',
            'first_name',
            'last_name',
            'payment_method',
            'delivery_method',
            'total_amount',
            'outstanding',
            'address',
            'total_amount',
            'ori_total_amount',
            'phone_no',
        ]