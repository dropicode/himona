from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Invoice, InvoiceDetails, PromoCode

# Register your models here.


class InvoiceAdmin(admin.ModelAdmin):
    list_display = ['invoice', 'username', 'created_at', 'outstanding', 'complete', 'remarks', 'remarks_unread']
    list_editable = ['outstanding', 'complete', 'remarks', 'remarks_unread']
    search_fields = ['invoice', 'username']
    list_filter = ['outstanding', 'complete', 'advice_slip', 'cancel', 'refund', 'payment_method', 'delivery_method', 'remarks_unread']


admin.site.register(Invoice, InvoiceAdmin)


class InvoiceDetailsAdmin(admin.ModelAdmin):
    list_display = ['details_invoice', 'username', 'product_id_unique', 'selling_price', 'quantity', 'preorder', 'ready', 'dispatched']
    list_editable = ['preorder', 'ready', 'dispatched']
    search_fields = ['details_invoice__invoice', 'username']
    list_filter = ['product_id_unique', 'preorder', 'ready', 'dispatched']


admin.site.register(InvoiceDetails, InvoiceDetailsAdmin)


class PromoCodeAdmin(admin.ModelAdmin):
    list_display = ['promo_code', 'percent_off', 'validity', 'one_off']
    list_editable = ['percent_off', 'validity', 'one_off']
    search_fields = ['promo_code']
    list_filter = ['percent_off', 'validity', 'one_off']


admin.site.register(PromoCode, PromoCodeAdmin)