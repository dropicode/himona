import sys
sys.path.append('../')
from django.shortcuts import render
from django.views import View
from productspage.models import ClothingProducts, CharacterProducts, NoncharacterProducts
from productspage.forms import AddProductToCart
from .forms import InvoiceIdGen, Invoice
from . import models
from itertools import chain
from operator import attrgetter
from django.shortcuts import HttpResponseRedirect


# Create your views here.


class CartpageView(View):
    template_name = "cartpage/cartpage.html"

    def get(self, request, *args, **kwargs):
        #request.session['cart_items'] = {
        #    form_product_id_unique: [form_product_id, form_color, form_size, form_selling_price, form_quantity]}
        try:
            cart_session = request.session['cart_items']
            list_cart = []
            for i in cart_session.keys():
                list_cart.append(i)

            list_for_total = []
            total = 0
            for j in cart_session.values():
                total = total + int(j[3])*int(j[4])

            #cart_items = cart_session.items()
            #for i in cart_session:
            #    list = []
            #    x = i.keys()
            #    y = i.items()
            #    list.append(x)
            #    list.append(y)
            #    z = [item for sublist in list for item in sublist]
            #    cart_items.append(z)
            clothingproduct = ClothingProducts.objects.order_by('-created_at').filter(product_id_unique__in=list_cart)
            characterproducts = CharacterProducts.objects.order_by('-created_at').filter(product_id_unique__in=list_cart)
            noncharacterproducts = NoncharacterProducts.objects.order_by('-created_at').filter(product_id_unique__in=list_cart)
            list = [clothingproduct, characterproducts, noncharacterproducts]
            count = 0
            for i in list:
                if len(i) == 0:
                    pass
                else:
                    if count == 0:
                        products = i
                        count = count + 1
                    else:
                        products = sorted(chain(products, i), key=attrgetter('created_at'))

            #products.union(characterproducts).objects.order_by('-created_at')

            if not 'cart_amount_total' in request.session or not request.session['cart_amount_total']:
                request.session['cart_amount_total'] = {'total': total,}
            else:
                saved_cart = request.session['cart_amount_total']
                saved_cart.update({'total': total,})
                request.session['cart_amount_total'] = saved_cart

            context = {
                'cart_session': cart_session,
                'products': products,
                'total': total,
            }
        except (UnboundLocalError, KeyError):
            context = {
            }
        return render(request, self.template_name, context)


class CartmodifypageView(View):
    template_name = "cartpage/cartmodify.html"
    form_class = AddProductToCart
    def get(self, request, *args, **kwargs):
        product_id_unique = str(self.kwargs['product_id_unique_slug'])
        form = AddProductToCart()
        product_id_slug = str(self.kwargs['product_id_slug']).lower()
        color = str(self.kwargs['color_slug']).lower()

        cart_session = request.session['cart_items']
        list_cart = []
        for i in cart_session.keys():
            list_cart.append(i)

        #products = Products.objects.order_by('-created_at').filter(product_id_unique__in=list)
        clothingproduct_unique = ClothingProducts.objects.order_by('-created_at').filter(product_id_unique=product_id_unique)
        characterproduct_unique = CharacterProducts.objects.order_by('-created_at').filter(product_id_unique=product_id_unique)
        list = [characterproduct_unique, clothingproduct_unique]
        for i in list:
            if len(i) == 0:
                pass
            else:
                product_unique = i

        clothingproduct = ClothingProducts.objects.order_by('-created_at').filter(product_id_unique__in=list_cart)
        characterproducts = CharacterProducts.objects.order_by('-created_at').filter(product_id_unique__in=list_cart)
        noncharacterproducts = NoncharacterProducts.objects.order_by('-created_at').filter(
            product_id_unique__in=list_cart)
        list = [clothingproduct, characterproducts, noncharacterproducts]
        count = 0
        for i in list:
            if len(i) == 0:
                pass
            else:
                if count == 0:
                    products = i
                    count = count + 1
                else:
                    products = sorted(chain(products, i), key=attrgetter('created_at'))

        form = AddProductToCart()
        context = {
            'form': form,
            'product_id_unique': product_id_unique,
            'cart_session': cart_session,
            'product_unique': product_unique,
            'products': products,

        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        form_product_id = form.data['product_id']
        form_color = form.data['color']
        form_size = form.data['size']
        form_quantity = int(form.data['quantity'])
        form_selling_price = form.data['selling_price']
        form_product_id_unique = form_product_id + form_color + form_size

        if form_quantity == 0:
            request.session.modified = True
            del request.session['cart_items'][form_product_id_unique]

        else:
            saved_cart = request.session['cart_items']
            saved_cart.update(
                {form_product_id_unique: [form_product_id, form_color, form_size, form_selling_price, form_quantity]})
            request.session['cart_items'] = saved_cart

        return HttpResponseRedirect('/cartpage/')


class CartcheckoutpageView(View):
    template_name = "cartpage/cartcheckout.html"
    form_class = InvoiceIdGen

    def get(self, request, *args, **kwargs):

        try:
            cart_session = request.session['cart_items']
            list_cart = []
            for i in cart_session.keys():
                list_cart.append(i)

            list_for_total = []
            total = 0
            for j in cart_session.values():
                total = total + int(j[3])*int(j[4])

            clothingproduct = ClothingProducts.objects.order_by('-created_at').filter(product_id_unique__in=list_cart)
            characterproducts = CharacterProducts.objects.order_by('-created_at').filter(
                product_id_unique__in=list_cart)
            noncharacterproducts = NoncharacterProducts.objects.order_by('-created_at').filter(
                product_id_unique__in=list_cart)
            list = [clothingproduct, characterproducts, noncharacterproducts]
            count = 0
            for i in list:
                if len(i) == 0:
                    pass
                else:
                    if count == 0:
                        products = i
                        count = count + 1
                    else:
                        products = sorted(chain(products, i), key=attrgetter('created_at'))

            if not 'cart_amount_total' in request.session or not request.session['cart_amount_total']:
                request.session['cart_amount_total'] = {'total': total,}
            else:
                saved_cart = request.session['cart_amount_total']
                saved_cart.update({'total': total,})
                request.session['cart_amount_total'] = saved_cart

        except KeyError:
            context = {
            }
            return render(request, self.template_name, context)

        form = InvoiceIdGen()
        username = request.session['username']
        total_amount = request.session['cart_amount_total']['total']
        context = {
            'form': form,
            'username': username,
            'total_amount': total_amount,
            'cart_session': cart_session,
            'products': products,
            'total': total,
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        ans = form.save(commit=False)
        cost = 0
        cart_items = request.session['cart_items']
        cart_session = request.session['cart_items']
        list_cart = []
        for i in cart_session.keys():
            list_cart.append(i)

        clothingproduct = ClothingProducts.objects.order_by('-created_at').filter(product_id_unique__in=list_cart)
        characterproducts = CharacterProducts.objects.order_by('-created_at').filter(
            product_id_unique__in=list_cart)
        noncharacterproducts = NoncharacterProducts.objects.order_by('-created_at').filter(
            product_id_unique__in=list_cart)
        list = [clothingproduct, characterproducts, noncharacterproducts]
        count = 0
        for i in list:
            if len(i) == 0:
                pass
            else:
                if count == 0:
                    products = i
                    count = count + 1
                else:
                    products = sorted(chain(products, i), key=attrgetter('created_at'))

        for key, val in cart_items.items():
            for i in products:
                if i.product_id_unique == key:
                    p0 = i
                    cost = cost + (int(p0.cost) * int(val[4]))
        ans.total_cost = cost
        ans.save()

        username = request.session['username']
        invoice_item = models.Invoice.objects.order_by('-created_at').filter(username=username)[0]
        invoice_item = invoice_item.invoice



        for key, val in cart_items.items():
            if key[0:3] == "FAS":
                Products = ClothingProducts
            elif key[0:3] == 'CHA':
                Products = CharacterProducts
            elif key[0:3] == 'NON':
                Products = NoncharacterProducts
            p2 = Products.objects.order_by('-created_at').filter(product_id_unique=key)[0]

            new_quantity = int(p2.stock) - int(val[4])
            if p2.stock == 0 and new_quantity < 0:
                p2.preorder = True
                p2.preorder_quantity = 0 - new_quantity
                p2.save()

                p3 = models.InvoiceDetails(
                    details_invoice=(Invoice.objects.get(invoice=invoice_item)), username=username,
                    product_id_unique=key,
                    product_id=val[0], color=val[1], size=val[2], selling_price=val[3],
                    quantity=(0 - new_quantity), ready=False, preorder=True,
                    total_cost=(int(p2.cost) * (0 - new_quantity))
                )
                p3.save()

            elif p2.stock > 0 and new_quantity > 0:
                p2.stock = new_quantity
                p2.save()

                p1 = models.InvoiceDetails(
                    details_invoice=(Invoice.objects.get(invoice=invoice_item)), username=username, product_id_unique=key,
                    product_id=val[0], color=val[1], size=val[2], selling_price=val[3],
                    quantity=val[4], ready=True, total_cost=(int(p2.cost) * int(val[4])),
                               )
                p1.save()

            elif p2.stock > 0 and new_quantity == 0:
                p2.preorder = True
                p2.stock = new_quantity
                p2.save()

                p1 = models.InvoiceDetails(
                    details_invoice=(Invoice.objects.get(invoice=invoice_item)), username=username,
                    product_id_unique=key,
                    product_id=val[0], color=val[1], size=val[2], selling_price=val[3],
                    quantity=val[4], ready=True, total_cost=(int(p2.cost) * int(val[4])),
                )
                p1.save()

            elif p2.stock > 0 and new_quantity < 0:
                p1 = models.InvoiceDetails(
                    details_invoice=(Invoice.objects.get(invoice=invoice_item)), username=username, product_id_unique=key,
                    product_id=val[0], color=val[1], size=val[2], selling_price=val[3],
                    quantity=int(p2.stock), ready=True, total_cost=(int(p2.cost) * int(p2.stock))
                )
                p1.save()

                p2.stock = new_quantity
                p2.preorder = True
                p2.preorder_quantity = 0 - new_quantity
                p2.save()

                p3 = models.InvoiceDetails(
                    details_invoice=(Invoice.objects.get(invoice=invoice_item)), username=username, product_id_unique=key,
                    product_id=val[0], color=val[1], size=val[2], selling_price=val[3],
                    quantity=(0-new_quantity), ready=False, preorder=True, total_cost=(int(p2.cost) * (0-new_quantity))
                )
                p3.save()

        del request.session['cart_items']
        return HttpResponseRedirect('/')
