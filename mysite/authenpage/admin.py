from django.contrib import admin

# Register your models here.
from django.contrib import admin

# Register your models here.

from .models import Profile


class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'email_confirmed']
    list_editable = ['email_confirmed']


admin.site.register(Profile, ProfileAdmin)