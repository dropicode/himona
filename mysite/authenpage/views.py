from django.shortcuts import render

# Create your views here.
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.db import IntegrityError
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.utils.encoding import force_text

from .forms import SignUpForm, LoginForm
from .token import account_activation_token

from django.conf import settings

from django.core.mail import send_mail
from django.views import View
from django.contrib import messages
import requests
import json

import sys
sys.path.append('../')



class SigninpageView(View):
    template_name = "authenpage/signinpage.html"
    form_class = LoginForm

    def get(self, request, *args, **kwargs):
        form = self.form_class()

        context = {
            'form': form,
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            # username = request.POST['username']
            # password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                request.session['username'] = username
                return redirect('/')
            else:
                context = {
                    'form': self.form_class(),
                }
                return render(request, self.template_name, context)

        else:
            form = LoginForm()

        context = {
            'form': form,
        }
        return render(request, self.template_name, context)


class SignoutpageView(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect('/')


class SignuppageView(View):
    template_name = "authenpage/signuppage.html"
    gender_list = ['women', 'men', 'both']
    lifestyle_list = ['kitchen', 'bathroom']
    form_class = SignUpForm

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        context = {
            'form': form,
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        template_name = "authenpage/signinpage.html"
        form = SignUpForm(request.POST)

        try:
            if form.is_valid():
                ''' Begin reCAPTCHA validation '''
                recaptcha_response = request.POST.get('g-recaptcha-response')
                data = {
                    'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                    'response': recaptcha_response
                }
                r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
                result = r.json()
                ''' End reCAPTCHA validation '''
                if result['success']:
                    user = form.save(commit=False)
                    user.is_active = False
                    user.save()
                    current_site = get_current_site(request)
                    subject = 'Activate Your MySite Account'
                    message = render_to_string('authenpage/account_activation_email.html', {
                        'user': user,
                        'domain': '192.168.1.134:80',
                        # 'domain': current_site.domain,
                        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                        'token': account_activation_token.make_token(user),
                    })
                    email_from = settings.EMAIL_HOST_USER
                    user_email = str(user.email)
                    recipient_list = [user_email, ]
                    send_mail(subject, message, email_from, recipient_list, auth_user=settings.EMAIL_HOST_USER,
                              auth_password=settings.EMAIL_HOST_PASSWORD)

                    context = {
                        'activation_sent': 'activation_sent'
                    }
                    messages.success(request, 'New comment added with success!')
                    return render(request, template_name, context)

                else:
                    context = {
                        'form': form,
                    }
                    messages.error(request, 'Invalid reCAPTCHA. Please try again.')
                    return render(request, self.template_name, context)

            else:
                messages.error(request, '123.')
                context = {
                    'form': form,
                }
                return render(request, self.template_name, context)
        except IntegrityError as err:
            context = {
                'form': form,
            }
            return render(request, self.template_name, context)


class AccountactivatePage(View):
    template_name = "authenpage/accountactivatepage.html"

    def get(self, request, uidb64, token):
        try:
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None

        if user is not None and account_activation_token.check_token(user, token):
            user.is_active = True
            user.profile.email_confirmed = True
            user.save()
            login(request, user)
            context = {
                'activated': 'activated',
            }
            return render(request, self.template_name, context)
        else:
            return render(request, self.template_name)