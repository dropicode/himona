from django.apps import AppConfig


class AuthenpageConfig(AppConfig):
    name = 'authenpage'
