from django.shortcuts import render
from django.views import View

# Create your views here.
class HomepageView(View):
    template_name = "homepage/homepage.html"

    def get(self, request, *args, **kwargs):
        request.session["current"] = "homepage"
        context = {

        }
        return render(request,  self.template_name, context)

class LanguagesessionView(View):
    template_name = "homepage/homepage.html"

    def get(self, request, *args, **kwargs):
        lang_slug = str(self.kwargs["lang_slug"]).lower()
        request.session["lang"] = lang_slug
        request.session["current"] = "homepage"
        context = {

        }
        return render(request,  self.template_name, context)


class NavpageView(View):

    def get(self, request, *args, **kwargs):
        nav_slug = str(self.kwargs["nav_slug"]).lower()
        request.session["current"] = nav_slug
        if nav_slug == "howto":
            template_name = "homepage/howtopage.html"
        if nav_slug == "comment":
            template_name = "homepage/commentpage.html"
        if nav_slug == "aboutus":
            template_name = "homepage/aboutuspage.html"
        if nav_slug == "privacyterms":
            template_name = "homepage/privacyterms.html"
        context = {

        }
        return render(request,  template_name, context)

